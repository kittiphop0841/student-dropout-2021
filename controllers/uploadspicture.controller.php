<?php
$studentuid = @$_POST['studentuid'];
$valid_ext = array("png","jpeg", "jpg"); // Valid image extension
$path = "../public/";  //BasicPath
@mkdir("../public/fileupload/",0755); //Create Path File fileupload
@mkdir("../public/fileupload/student/",0755); //Create Path File fileupload
@mkdir("../public/fileupload/address/",0755); //Create Path File fileupload

//File Address Student
if (isset($_FILES['addpic'])) {
    $countfiles = count(@$_FILES['addpic']['name']);
    $upload_location = "uploads/";
    $files_arr = array();
    for ($index = 0; $index < $countfiles; $index++) {
        if (isset($_FILES['addpic']['name'][$index]) && $_FILES['addpic']['name'][$index] != '') {
            $filename = $_FILES['addpic']['name'][$index];
            $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
            if (in_array($ext, $valid_ext)) {
                $pathAddstu = $path . "fileupload/address/" . $studentuid."_".$index.".png";
                if (move_uploaded_file(@$_FILES['addpic']['tmp_name'][$index], $pathAddstu)) {
                    $files_arr[] = $pathAddstu;
                }
            }else{
                echo "NT";
            } 
        }
    }
}

//File Student
if (isset($_FILES['picstudent'])) {
    $upload = $_FILES['picstudent'];
    $pathPicStudent = $path . "fileupload/student/";
    $newname = $upload['name'];
    $ext = strtolower(pathinfo($newname, PATHINFO_EXTENSION));
    if (in_array($ext, $valid_ext)) {
        $path_copy = $pathPicStudent . $studentuid.".png";
        move_uploaded_file(@$_FILES['picstudent']['tmp_name'], $path_copy);
    }else{
        echo "NT";
    } 
}

?>