<?php
session_start();
include "../configs/db.php";
include "../PHPExcel/Classes/PHPExcel.php";
include "user.queryexport.controller.php";
$con = OpenCon();
$obj = new Userexport();

$statusfind = isset($_POST['statusfind']) ? $_POST['statusfind']:$_GET['statusfind'];
$statusstudent = isset($_POST['statusstudent']) ? $_POST['statusstudent']:$_GET['statusstudent'];
$level = isset($_POST['level']) ? $_POST['level']:$_GET['level'];
$yearofterm = isset($_POST['yearofterm']) ? $_POST['yearofterm']:$_GET['yearofterm'];
$term = isset($_POST['term']) ? $_POST['term']:$_GET['term'];
$province = isset($_POST['province']) ? $_POST['province']:$_GET['province'];
$schoolname = isset($_POST['schoolname']) ? $_POST['schoolname']:$_GET['schoolname'];
$querysh="";
$sortbyupdate = isset($_POST['sortbyupdate']) ? $_POST['sortbyupdate']:$_GET['sortbyupdate'];
$sortOrder='';

if (@$_POST["yearofterm"] && @$_POST["term"]) {
    $yearofterm = $_POST["yearofterm"];
    $term = $_POST["term"];
    $nameDatabase = 'totalstudent' . $yearofterm . '_' . $term;
} else {
    $yearofterm = date("Y") + 543;
    $term = '1';
    $nameDatabase = 'totalstudent' . $yearofterm ."_".$term;
}

if($_SESSION['leveluid'] == '1' || $_SESSION['leveluid'] == '0' || $_SESSION['leveluid'] == '3'){
    if ($statusstudent != 'All') {
        $querysh = " studentstaus_id = '{$statusstudent}'";
    }else{
        $querysh = " studentstaus_id IN ('001', '002', '003', '005', '007', '008', '004', '006', '009')";
    }

    if ($schoolname) {
        $querysh .= " AND school_id = '$schoolname'";
    }
}else{
    if ($_SESSION['username']) {
        $querysh = "school_id='" . $_SESSION['username'] . "'";
    }
    
    if ($statusstudent) {
        $querysh .= " AND studentstaus_id = '{$statusstudent}'";
    }
}

if ($statusfind== '1') {
    $querysh .= " AND studentstaus_id IN ('004', '006')";
} else if ($statusfind == '2') {
    $querysh .= " AND studentstaus_id IN ('001', '002', '003', '005', '007', '008')";
}

if (@$level) {
    $querysh .= " AND educationlevel_id = '$level'";
}

if ($province) {
    $querysh .= " AND province_id = '$province'";
}

$where = '';
if($querysh != ''){
$where = ' WHERE ';
}

if($sortbyupdate == 'false'){
    $sortOrder=" firstname ASC ";
}else{
    $sortOrder=" updated DESC ";
}

$sql = "SELECT * FROM $nameDatabase WHERE $querysh ORDER BY $sortOrder ";
$result = $con->query($sql);
$excel = new PHPExcel();

// insert someData
// $style = array(
//     'alignment' => array(
//         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
//         'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
//     ),
// );

// $excel->getDefaultStyle()->applyFromArray($style);

$excel->getActiveSheet()
    ->getStyle('B')
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

$excel->getActiveSheet()
    ->getStyle('V')
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

$excel->getActiveSheet()
    ->getStyle('W')
    ->getNumberFormat()
    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);

$excel->getActiveSheet()
    ->getStyle('A1:AQ1')
    ->getFill()
    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()
    ->setRGB('4DC46F');

// $excel->getActiveSheet()
//     ->getStyle('B3:G3')
//     ->getFill()
//     ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
//     ->getStartColor()
//     ->setRGB('4DC46F');

// $excel->getActiveSheet()
//     ->getStyle('A25:A27')
//     ->getFill()
//     ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
//     ->getStartColor()
//     ->setRGB('4DC46F');

$excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('D')->setWidth(19);
$excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$excel->getActiveSheet()->getColumnDimension('F')->setWidth(18);
$excel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$excel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
$excel->getActiveSheet()->getColumnDimension('I')->setWidth(8);
$excel->getActiveSheet()->getColumnDimension('J')->setWidth(11);
$excel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
$excel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
$excel->getActiveSheet()->getColumnDimension('M')->setWidth(65);
$excel->getActiveSheet()->getColumnDimension('N')->setWidth(65);

$excel->getActiveSheet()->getColumnDimension('O')->setWidth(12);
$excel->getActiveSheet()->getColumnDimension('P')->setWidth(12);
$excel->getActiveSheet()->getColumnDimension('Q')->setWidth(18);
$excel->getActiveSheet()->getColumnDimension('R')->setWidth(30);
$excel->getActiveSheet()->getColumnDimension('S')->setWidth(28);
$excel->getActiveSheet()->getColumnDimension('T')->setWidth(42);
$excel->getActiveSheet()->getColumnDimension('U')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('V')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('W')->setWidth(25);

$excel->getActiveSheet()->getColumnDimension('X')->setWidth(26);
$excel->getActiveSheet()->getColumnDimension('Y')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('Z')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('AA')->setWidth(28);
$excel->getActiveSheet()->getColumnDimension('AB')->setWidth(21);
$excel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('AD')->setWidth(35);
$excel->getActiveSheet()->getColumnDimension('AE')->setWidth(17);
$excel->getActiveSheet()->getColumnDimension('AF')->setWidth(18);
$excel->getActiveSheet()->getColumnDimension('AG')->setWidth(18);
$excel->getActiveSheet()->getColumnDimension('AH')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('AI')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(30);
$excel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
$excel->getActiveSheet()->getColumnDimension('AM')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('AN')->setWidth(25);
$excel->getActiveSheet()->getColumnDimension('AO')->setWidth(30);
$excel->getActiveSheet()->getColumnDimension('AP')->setWidth(40);
$excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(40);


// $excel->getDefaultStyle()->getFont('A1:G22')->setName('Arial');
// $styleArray = array(
//     'borders' => array(
//         'allborders' => array(
//             'style' => PHPExcel_Style_Border::BORDER_THIN,
//         ),
//     ),
// );

// $excel->getActiveSheet()->getStyle('A2:G27')->applyFromArray($styleArray);
$excel->getActiveSheet()->setTitle("ReportStudent");

//Set หัว Column
$excel->setActiveSheetIndex(0)

->setCellValue('A1', 'วันที่อัปเอตข้อมูล')
->setCellValue('B1', 'รหัสนักเรียน')
->setCellValue('C1', 'รหัสประจำตัวประชาชน')
->setCellValue('D1', 'เลขที่หนังสือเดินทาง')
->setCellValue('E1', 'คำนำชื่อ')
->setCellValue('F1', 'ชื่อ')
->setCellValue('G1', 'ชื่อกลาง')
->setCellValue('H1', 'นามสกุล')
->setCellValue('I1', 'เพศ')
->setCellValue('J1', 'ปีการศึกษา')
->setCellValue('K1', 'ภาคการศึกษา')
->setCellValue('L1', 'วันเดือนปีเกิด')
->setCellValue('M1', 'หน่วยงานสำนักงาน กศน')
->setCellValue('N1', 'สถานศึกษา/หน่วยงาน')
->setCellValue('O1', 'สัญชาติ')
->setCellValue('P1', 'ความพิการ')
->setCellValue('Q1', 'ความด้อยโอกาส')

->setCellValue('R1', 'ประจำบ้าน (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('S1', 'เลขที่บ้าน (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('T1', 'หมู่บ้าน/คอนโด/อพาร์ทเม้นท์ (ตามทะเบียนบ้าน)')
->setCellValue('U1', 'ซอย (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('V1', 'ถนน (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('W1', 'จังหวัด (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('X1', 'อำเภอ (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('Y1', 'ตำบล (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('Z1', 'ละติจูด (ที่อยู่ตามทะเบียนบ้าน)')
->setCellValue('AA1', 'ลองจิจูด (ที่อยู่ตามทะเบียนบ้าน)')

->setCellValue('AB1', 'ประจำบ้าน (ที่อยู่ปัจจุบัน)')
->setCellValue('AC1', 'เลขที่บ้าน (ที่อยู่ปัจจุบัน)')
->setCellValue('AD1', 'หมู่บ้าน/คอนโด/อพาร์ทเม้นท์ (ที่อยู่ปัจจุบัน)')
->setCellValue('AE1', 'หมู่ที่ (ที่อยู่ปัจจุบัน)')
->setCellValue('AF1', 'ซอย (ที่อยู่ปัจจุบัน)')
->setCellValue('AG1', 'ถนน (ที่อยู่ปัจจุบัน)')
->setCellValue('AH1', 'จังหวัด (ที่อยู่ปัจจุบัน)')
->setCellValue('AI1', 'อำเภอ (ที่อยู่ปัจจุบัน)')
->setCellValue('AJ1', 'ตำบล (ที่อยู่ปัจจุบัน)')
->setCellValue('AK1', 'ละติจูด (ที่อยู่ปัจจุบัน)')
->setCellValue('AL1', 'ลองจิจูด (ที่อยู่ปัจจุบัน)')
->setCellValue('AM1', 'สถานะติดตาม')
->setCellValue('AN1', 'สถานะนักเรียน')
->setCellValue('AO1', 'โน๊ต')
->setCellValue('AP1', 'โรงเรียนต้นสังกัด')
->setCellValue('AQ1', 'ต้นสังกัด');


//เขียนข้อมูล
$rowCell=2;
$c=0; 
while($row = $result->fetch_assoc()){
    $objData = $obj->queryUserdetails($row["student_id"]);
    $dataStudent = json_decode($objData);
    if($row["gender_id"] == '1'){
        $gender_name = 'ชาย';
    }else{
        $gender_name = 'หญิง';
    }

    $excel->setActiveSheetIndex(0)
    ->setCellValue('A'.$rowCell, $row["updated"])
    ->setCellValue('B'.$rowCell, $row["student_id"])
    ->setCellValue('C'.$rowCell, $row["person_id"])
    ->setCellValue('D'.$rowCell, $row["passportnumber"])
    ->setCellValue('E'.$rowCell, $row["prefix_id"]." - ".$dataStudent->{'prefixName'})
    ->setCellValue('F'.$rowCell, $row["firstname"])
    ->setCellValue('G'.$rowCell, $row["middlename"])
    ->setCellValue('H'.$rowCell, $row["lastname"])
    ->setCellValue('I'.$rowCell, $row["gender_id"]." - ".$gender_name)
    ->setCellValue('J'.$rowCell, $row["academicyear"])
    ->setCellValue('K'.$rowCell, $row["semester"])
    ->setCellValue('L'.$rowCell, $row["birthdate"])
    ->setCellValue('M'.$rowCell, $row["organize_id"]." - ".$dataStudent->{'organizeName'})
    ->setCellValue('N'.$rowCell, $row["school_id"] ." - ".$dataStudent->{'organizeName'})
    ->setCellValue('O'.$rowCell, $row["nationality_id"]." - ".$dataStudent->{'nationality_name'})
    ->setCellValue('P'.$rowCell, $row["disability_id"]." - ".$dataStudent->{'disability_name'})
    ->setCellValue('Q'.$rowCell, $row["disadvantageeducation_id"]." - ".$dataStudent->{'disadvantageeducation_name'})

    ->setCellValue('R'.$rowCell, $row["house_id"])
    ->setCellValue('S'.$rowCell, $row["house"])
    ->setCellValue('T'.$rowCell, $row["other_addres_home"])
    ->setCellValue('U'.$rowCell, $row["soi"])
    ->setCellValue('V'.$rowCell, $row["street"])
    ->setCellValue('W'.$rowCell, $row["province_id"]." - ".$dataStudent->{'province'})
    ->setCellValue('X'.$rowCell, $row["district_id"]." - ".$dataStudent->{'district'})
    ->setCellValue('Y'.$rowCell, $row["subdistrict_id"]." - ".$dataStudent->{'subdistrict'})
    ->setCellValue('Z'.$rowCell, $row["latitude"])
    ->setCellValue('AA'.$rowCell, $row["longitude"])
   
    ->setCellValue('AB'.$rowCell, $row["present_house_id"])
    ->setCellValue('AC'.$rowCell, $row["present_house"])
    ->setCellValue('AD'.$rowCell, $row["other_addres_present"])
    ->setCellValue('AE'.$rowCell, $row["present_villagenumber"])
    ->setCellValue('AF'.$rowCell, $row["present_soi"])
    ->setCellValue('AG'.$rowCell, $row["present_street"])
    ->setCellValue('AH'.$rowCell, $row["present_province_id"]." - ".$dataStudent->{'provinceCurrent'})
    ->setCellValue('AI'.$rowCell, $row["present_district_id"]." - ".$dataStudent->{'districtCurrent'})
    ->setCellValue('AJ'.$rowCell, $row["present_subdistrict_id"]." - ".$dataStudent->{'subdistrictCurrent'})
    ->setCellValue('AK'.$rowCell, $row["present_latitude"])
    ->setCellValue('AL'.$rowCell, $row["present_longitude"])

    ->setCellValue('AM'.$rowCell, $dataStudent->{'status'})
    ->setCellValue('AN'.$rowCell, $row["studentstaus_id"]." - ".$dataStudent->{'statusStudent'})
    ->setCellValue('AO'.$rowCell, $dataStudent->{'statusnote'})
    ->setCellValue('AP'.$rowCell, $row["schooltype_id"]." - ".$dataStudent->{'schooltypeName'})
    ->setCellValue('AQ'.$rowCell, $row["jurisdiction_id"]." - ".$dataStudent->{'jurisdictionName'});
   
    
    
 
    

	$rowCell++;
}


header('Content-Type: application/xls');
header("Content-Disposition: attachment; filename=รายงานนักเรียน.xlsx");
header("Cache-Control: max-age=0");

// write File
$file = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
$file->save('php://output');
