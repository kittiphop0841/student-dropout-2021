/CUT/
<?
session_start();
ob_start();
?>
<div class="container">
  <div class="row">
    <div class="col-md-9 col-xs-12">

      <form name="register" action="register_db.php" method="POST" id="register" class="form-horizontal">
        <div class="form-group">
          <h4>
            <div class="col-sm-3" align="right"> หน้าเปลี่ยนรหัสผ่าน
          </h4>
        </div>
        <div>
          <div class="form-group">
            <div class="col-sm-3" align="right"> ยืนยันรหัสเก่า : </div>
            <div class="col-sm-4" align="left">
              <input name="passwordold" type="text" class="form-control" id="passwordold" placeholder="กรอกรหัสผ่านเก่า" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3" align="right"> รหัสผ่านใหม่ : </div>
            <div class="col-sm-4" align="left">
              <input name="passwordnew" type="text" class="form-control" id="passwordnew" placeholder="กรอกรหัสผ่านใหม่" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3" align="right"> ยืนยันรหัสผ่านใหม่ : </div>
            <div class="col-sm-4" align="left">
              <input name="passwordconfirm" type="text" class="form-control" id="passwordconfirm" placeholder="กรอกยืนยันรหัสผ่านใหม่อีกครั้ง" title="ภาษาอังกฤษหรือตัวเลขเท่านั้น" minlength="2" />
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-5" align="center">
              <input type="button" class="btn btn-success" value="ยืนยัน" onclick="check()" />
            </div>
          </div>
      </form>
      <div id="statusupdatepass"></div>
    </div>
  </div>
</div>

<script language="javascript">
  function check() {
    let passwordold = $('#passwordold').val();
    let passwordnew = $('#passwordnew').val();
    let passwordconfirm = $('#passwordconfirm').val();

    if (passwordold == "") {
      alert('กรุณากรอก PASSWORD เก่า');
      return false;
    } else if (passwordnew == "") {
      alert('กรุณากรอก PASSWORD ใหม่');
      return false;
    } else if (passwordconfirm == "") {
      alert('กรุณายืนยัน PASSWORD');
      return false;
    } else if (passwordnew != passwordconfirm) {
      alert('คุณใส่ password ไม่ตรงกัน');
      return false;
    }

    let data = {
      passwordnew: $('#passwordnew').val(),
      passwordold: $('#passwordold').val(),
    }

    $.ajax({
      url: "../../controllers/edituser.controller.php",
      method: "POST",
      data: data,
      success: function(response) {
        let statussplit = response.split(":");
        if (statussplit[0] == 'S') {
          $('#statusupdatepass').html('<div class="alert alert-success" role="alert">บันทึกรหัสผ่านใหม่สำเร็จ</div>');
          setTimeout(function() {
            window.location.replace("mainapp.php");
          }, 1500)
        } else if (statussplit[0] == 'W') {
          $('#statusupdatepass').html('<div class="alert alert-danger" role="alert">รหัสผ่านไม่ถูกต้อง ไม่มี User นี้ในระบบ</div>');
        }
      }
    });
  }
</script>