/CUT/
<link rel="stylesheet" type="text/css" href="../showalldetails/showdetails.css"/>
<?php
session_start();
include "../../configs/db.php";
$con = OpenCon();
include_once "../../models/querycommon.php";
$resultPro = queryprovince();
$resultOrganize = queryOrganize();
$currentyear = date("Y") + 543;
?>
<div class="mainshowdatatable">
    <div class="subdetail">
        <div style="background: #FFFF99; padding-inline :10px 10px">
            <h4><b>หลุดจากระบบ (<span id="sumboxfirst1" style="font-size: small;"></span>) <img src="../../public/people2.png" align="right" width="55" height="55" VSPACE="10" HSPACE="10" /></b></h4>
            <div id="dropstudent"></div>
        </div>
        <div style="background: #66FFCC; padding-inline :10px 10px">
            <h4><b>ตามแล้วพบตัว (<span id="sumboxfirst2" style="font-size: small;"></span>)<img src="../../public/fine.png" align="right" width="55" height="55" VSPACE="10" HSPACE="10" /></b></h4>
            <div id="tracktudentfind"></div>
        </div>
        <div style="background:	#FA8072; padding-inline :10px 10px">
            <h4><b>ตามแล้วไม่พบตัว (<span id="sumboxfirst3" style="font-size: small;"></span>)<img src="../../public/nonfine.png" align="right" width="55" height="55" VSPACE="10" HSPACE="10" /></b></h4>
            <div id="tracktudentnotfind"></div>
        </div>
    </div>
    <!-- level 3 or 4 -->
    <div class="subdetail" <?php if ($_SESSION['leveluid'] == '4') {
                                echo 'style="display:none"';
                            } ?>>
        <div style="background: #FFFF99; padding-inline :10px 10px">
            <h4><b>หลุดจากระบบ (<span id="sumbox1" style="font-size: small;"></span>) <img src="../../public/people2.png" align="right" width="55" height="55" VSPACE="10" HSPACE="10" /></b></h4>
            <div id="dropstudentOther"></div>
        </div>
        <div style="background: #66FFCC; padding-inline :10px 10px">
            <h4><b>ตามแล้วพบตัว (<span id="sumbox2" style="font-size: small;"></span>) <img src="../../public/fine.png" align="right" width="55" height="55" VSPACE="10" HSPACE="10" /></b></h4>
            <div id="tracktudentfindOther"></div>
        </div>
        <div style="background:	#FA8072; padding-inline :10px 10px">
            <h4><b>ตามแล้วไม่พบตัว (<span id="sumbox3" style="font-size: small;"></span>) <img src="../../public/nonfine.png" align="right" width="55" height="55" VSPACE="10" HSPACE="10" /></b></h4>
            <div id="tracktudentnotfindOther"></div>
        </div>
    </div>
    <div>
        <div <?php if ($_SESSION['leveluid'] != '4') {
                    echo 'style="display:none"';
                } ?>>
            <form id="data" action="#">
                ระดับชั้น :
                <select name="level" id="level">
                    <option value=''>---กรุณาเลือก---</option>
                    <option value="11">ประถมศึกษา</option>
                    <option value="12">มัธยมศึกษาตอนต้น</option>
                    <option value="13">มัธยมศึกษาตอนปลาย</option>
                </select>
                &nbsp;&nbsp;&nbsp;
                สาเหตุ :
                <select name="statusstudent" id="statusstudent">
                    <option value=''>---กรุณาเลือก---</option>
                    <option value="009" <?php if ($_SESSION['leveluid'] == '4') {echo 'selected';}?>>หลุดจากระบบการศึกษา</option>
                    <option value="001">กำลังศึกษา</option>
                    <option value="002">พักการเรียน</option>
                    <option value="003">เสียชีวิต</option>
                    <option value="004">อยู่ระหว่างดำเนินการติดตาม</option>
                    <option value="005">ลาออก</option>
                    <option value="006">พ้นสภาพ</option>
                    <option value="007">สำเร็จการศึกษา</option>
                    <option value="008">ไม่ประสงค์เรียนต่อ</option>
                </select>
                &nbsp;&nbsp;&nbsp;
                สถานะการติดตาม :

                <select name="statusfind" id="statusfind">
                    <option value=''>---กรุณาเลือก---</option>
                    <option value="1">ติดตามแล้วไม่พบตัว</option>
                    <option value="2">ติดตามแล้วพบตัว</option>

                </select>
                &nbsp;&nbsp;&nbsp;
                เทอม / ปี:
                <select name="term" id="term">
                    <option value=''>เลือกภาคการศึกษา</option>
                    <option value="1" selected>1</option>
                    <option value="2">2</option>
                </select>
                <select name="yearofterm" id="yearofterm">
                    <option value=''>เลือกปีการศึกษา</option>
                    <option value="2562" <?php if ($currentyear == '2562') {
                                                echo 'selected';
                                            } ?>>2562</option>
                    <option value="2563" <?php if ($currentyear == '2563') {
                                                echo 'selected';
                                            } ?>>2563</option>
                    <option value="2564" <?php if ($currentyear == '2564') {
                                                echo 'selected';
                                            } ?>>2564</option>
                    <option value="2565" <?php if ($currentyear == '2565') {
                                                echo 'selected';
                                            } ?>>2565</option>
                    <option value="2566" <?php if ($currentyear == '2566') {
                                                echo 'selected';
                                            } ?>>2566</option>
                    <option value="2567" <?php if ($currentyear == '2567') {
                                                echo 'selected';
                                            } ?>>2567</option>
                    <option value="2568" <?php if ($currentyear == '2568') {
                                                echo 'selected';
                                            } ?>>2568</option>
                    <option value="2569" <?php if ($currentyear == '2569') {
                                                echo 'selected';
                                            } ?>>2569</option>
                </select>
                <input type="checkbox" id="sortbyupdatelevel4"/> เรียงตามวันที่แก้ล่าสุด &nbsp;&nbsp;</br>
                <input type="button" value="ค้นหา" onclick="SelectFilter()" />
            </form>
            <div style="display:flex; justify-content: space-between;">
                <div><button onclick="backtable()">ก่อนหน้า</button></div>
                <div>หน้า <span id="countsum">1</span> จาก <span id="allofpageschool"></span></div>
                <div><button onclick="nexttable()">ถัดไป</button></div>
            </div>
        </div>
        <!-- level 3 or 4 -->
        <div <?php if ($_SESSION['leveluid'] == '4') {
                    echo 'style="display:none"';
                } ?>>
            <form id="data" action="#">
                <div class="filter">
                    <div>
                        ระดับชั้น :
                        <select name="levelForLevel" id="levelForLevel">
                            <option value=''>---กรุณาเลือก---</option>
                            <option value="11">ประถมศึกษา</option>
                            <option value="12">มัธยมศึกษาตอนต้น</option>
                            <option value="13">มัธยมศึกษาตอนปลาย</option>
                        </select>
                    </div>
                    <div>
                        สาเหตุ :
                        <select name="statusstudentForLevel" id="statusstudentForLevel">
                            <option value=''>---กรุณาเลือก---</option>
                            <option value="All">ทุกสถานะ</option>
                            <option value="009" <?php if ($_SESSION['leveluid'] != '4') {echo 'selected';}?>>หลุดจากระบบการศึกษา</option>
                            <option value="001">กำลังศึกษา</option>
                            <option value="002">พักการเรียน</option>
                            <option value="003">เสียชีวิต</option>
                            <option value="004">อยู่ระหว่างดำเนินการติดตาม</option>
                            <option value="005">ลาออก</option>
                            <option value="006">พ้นสภาพ</option>
                            <option value="007">สำเร็จการศึกษา</option>
                            <option value="008">ไม่ประสงค์เรียนต่อ</option>
                        </select>
                    </div>
                    <div>
                        สถานะการติดตาม :

                        <select name="statusfindForLevel" id="statusfindForLevel">
                            <option value=''>---กรุณาเลือก---</option>
                            <option value="1">ติดตามแล้วไม่พบตัว</option>
                            <option value="2">ติดตามแล้วพบตัว</option>

                        </select>
                    </div>
                    <div>
                        เทอม / ปี:
                        <select name="termForLevel" id="termForLevel">
                            <option value=''>เลือกภาคการศึกษา</option>
                            <option value="1" selected>1</option>
                            <option value="2">2</option>
                        </select>
                        <select name="yearoftermForLevel" id="yearoftermForLevel">
                            <option value=''>เลือกปีการศึกษา</option>
                            <option value="2562" <?php if ($currentyear == '2562') {
                                                        echo 'selected';
                                                    } ?>>2562</option>
                            <option value="2563" <?php if ($currentyear == '2563') {
                                                        echo 'selected';
                                                    } ?>>2563</option>
                            <option value="2564" <?php if ($currentyear == '2564') {
                                                        echo 'selected';
                                                    } ?>>2564</option>
                            <option value="2565" <?php if ($currentyear == '2565') {
                                                        echo 'selected';
                                                    } ?>>2565</option>
                            <option value="2566" <?php if ($currentyear == '2566') {
                                                        echo 'selected';
                                                    } ?>>2566</option>
                            <option value="2567" <?php if ($currentyear == '2567') {
                                                        echo 'selected';
                                                    } ?>>2567</option>
                            <option value="2568" <?php if ($currentyear == '2568') {
                                                        echo 'selected';
                                                    } ?>>2568</option>
                            <option value="2569" <?php if ($currentyear == '2569') {
                                                        echo 'selected';
                                                    } ?>>2569</option>
                        </select>
                    </div>
                    <div>
                        เลือกจังหวัด :
                        <select <?php if ($_SESSION['leveluid'] == '3') {
                                    echo 'disabled';
                                } ?> name="province" id="province" onchange="addressSel('shcool', 'mainsh')">
                            <option value=''>---กรุณาเลือก---</option>
                            <?php
                            if ($resultPro->num_rows > 0) {
                                foreach($resultPro as $rowPro){
                            ?>
                                    <option value="<?= $rowPro["province_id"] ?>" <?php if ($rowPro["province_id"] == $_SESSION['provinceuid']) {
                                                                                        echo 'selected';
                                                                                    } ?>><?= $rowPro["province_name"] ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div <?php if ($_SESSION['leveluid'] == '4' || $_SESSION['leveluid'] == '3') {echo 'style="display:none"';} ?>>
                        ชื่อโรงเรียน :
                        <select name="organize" id="organize">
                        </select>
                    </div>
                    <div <?php if (($_SESSION['leveluid'] == '1' || $_SESSION['leveluid'] == '0') && $_SESSION['leveluid'] != '3') {echo 'style="display:none"';} ?>>
                        ชื่อโรงเรียน :
                        <select name="organize" id="organizePro">
                            <option value=''>---กรุณาเลือก---</option>
                            <?php
                            if ($resultOrganize->num_rows > 0) {
                                foreach($resultOrganize as $rowOrganize){
                            ?>
                                    <option value="<?= $rowOrganize["organize_id"] ?>"><?= $rowOrganize["organize_name"] ?></option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <input type="hidden" id="leveluid" value="<?= $_SESSION['leveluid'] ?>" />
                <input type="checkbox" id="sortbyupdate" value="sortbyupdate"/> เรียงตามวันที่แก้ล่าสุด &nbsp;&nbsp;</br>
                <input type="button" value="ค้นหา" onclick="SelectFilterForOrherlevel()" />
            </form>
            <div style="display:flex; justify-content: space-between;">
                <div><button onclick="backtable()">ก่อนหน้า</button></div>
                <div>หน้า <span id="countsumlevel">1</span> จาก <span id="allofpage"></span></div>
                <div><button onclick="nexttable()">ถัดไป</button></div>
            </div>
        </div>
        <hr>
        <div id="loaderDiv" style="display: flex;width: 100%;justify-content: center;"><div class="loader"></div></div>
        <div id="showDataStudent"></div>
        
    </div>
</div>

<script type="text/javascript" src="../showalldetails/showalldetails.js"></script>